<?php

if ( $_POST ):
    
    ////////// CONFIGURATION //////////

    $to_Email = "contact@sevenz-consultant.com";
    $to_Name  = "From Website";           // PUT RECIPIENTS NAME HERE
    $siteName = "Sevenz-Consultant";           // PUT YOUR SITE NAME HERE
    $formText = true;

    /* HERE YOU CAN TRANSLATE THE VARIOUS TEXT MESSAGES RETURNED FROM THE SCRIPT. */

    $messages = array(
      "non_ajax"      => "Request must come from Ajax",
      "empty_fields"  => "Input fields are empty!",
      "invalid_email" => "Please enter a valid email!",
      "short_message" => "Too short message! Please enter something.",
      "thank_you"     => "Thank you %s! Your message was successfully sent.",
      "mail_error"    => "Could not send mail! Please check your PHP mail configuration.",
      "mail_success"  => "Thank you %s! Your message was successfully sent.",
      "contact_form"  => "[This message was sent from " . $siteName . "]"
     );

    ////////// MAIN CODE //////////

    header('Content-type: application/json');

    if( !isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        $output = json_encode(
        array(
            'type' => 'error', 
            'text' => $messages['non_ajax']
        ));
        die( $output );
    } 

    if( !isset($_POST["userEmail"]) || !isset($_POST["userName"]) || !isset($_POST["userSubject"]) || !isset($_POST["userMessage"]))
    {
        $output = json_encode(array('type'=>'error', 'text' => $messages['empty_fields'] ));
        die( $output );
    }
    
    // DETECT & PREVENT FROM HEADER INJECTIONS
     $malicious = "/(content-type|bcc:|cc:|to:|href)/i";
     foreach ( $_POST as $key => $val ) {
         if ( preg_match( $malicious, $val ) ) {
           exit( 'FAILURE' );
       }

    }

    $user_Name        = filter_var( trim($_POST["userName"]), FILTER_SANITIZE_STRING );
    $user_Email       = filter_var( trim($_POST["userEmail"]), FILTER_SANITIZE_EMAIL );
    $user_Subject     = filter_var( trim($_POST["userSubject"]), FILTER_SANITIZE_STRING );
    $user_Message     = filter_var( trim($_POST["userMessage"]), FILTER_SANITIZE_STRING );
    
    if( !filter_var($user_Email, FILTER_VALIDATE_EMAIL) ) 
    {
        $output = json_encode(array('type'=>'error', 'text' => sprintf( $messages['invalid_email'], $user_Name ) ));
        die($output);
    }
    if( strlen( $user_Message ) < 5 ) //CHECK FOR EMPTY OR SHORT MESSAGE
    {
        $output = json_encode(array('type'=>'error', 'text' => sprintf( $messages['short_message'] ) ));
        die($output);
    }
   
    require 'phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;

    /* USE GMAIL SMTP SERVER 

        ATTENTION: Before using Gmail's SMTP service for your contact form you have to
        allow access for a new application by going to the following address:
        https://accounts.google.com/DisplayUnlockCaptcha

        USAGE:  1) Remove the two lines denoted by 'REMOVE TO USE GMAIL OR THIRD PARTY SMTP'
                2) Enter your Username and Password in the respective fields
    */

    /* REMOVE TO USE GMAIL OR THIRD PARTY SMTP

    $mail->isSMTP();                                    // SET MAILER TO USE SMTP
    $mail->Host         = 'smtp.gmail.com';             // SPECIFY MAIN AND BACKUP SERVERS
    $mail->SMTPAuth     = true;                         // ENABLE SMTP AUTHENTICATION
    $mail->Username     = 'youremail@gmail.com';        // SMTP USERNAME
    $mail->Password     = "password";                   // SMTP PASSWORD
    $mail->SMTPSecure   = 'tls';                        // ENABLE ENCRYPTION 'ssl' ALSO ACCEPTED
    $mail->Port         = 587;

    REMOVE TO USE GMAIL OR THIRD PARTY SMTP */

    /* UNCOMMENT THE FOLLOWING 2 LINES TO ENABLE EMAIL DEBUGGING */
    // $mail->SMTPDebug = 2;
    // $mail->Debugoutput = 'html';

    /* SEND EMAIL USING SENDMAIL */
    // $mail->isSendmail();

    $mail->SetFrom( $user_Email, $user_Name);               // ADD SENDER ADDRESS AND NAME
    $mail->addAddress( $to_Email, $to_Name);                // ADD A RECIPIENT. NAME IS OPTIONAL.
    $mail->addReplyTo( $user_Email, $user_Name);            // ADD Reply To FIELD
    // $mail->addAddress('ellen@example.com');              // ADD ANOTHER RECIPIENT
    // $mail->addCC('cc@example.com');                      // ADD CC RECIPIENTS
    $mail->addBCC('ihsan.1x4n@gmail.com');                  // ADD BCC RECIPIENTS

    $mail->isHTML(true);                                    // SET EMAIL FORMAT TO HTML
    $mail->CharSet = "UTF-8";    
    $mail->Subject = $user_Subject;
    $mail->Body    = ($formText)? $user_Message . "\n\r<br/><br/>" . $messages['contact_form'] : $user_Message;

    /* SEND PLAINTEXT MESSAGE*/
    // $mail->AltBody = $user_Message;

    if( ! $mail->send() )
    {
        $output = json_encode(array('type'=>'error', 'text' => sprintf( $messages['mail_error'] . "Error:" . $mail->ErrorInfo ) ));
        die( $output );

    } else {

        $output = json_encode(array('type'=>'message', 'text' => sprintf( $messages['mail_success'], $user_Name ) ));
        die( $output );

    }

endif;
