<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <title>Sevenz Consultant | One stop IT solution for your business</title>
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <meta http-equiv="content-language" content="en-us">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="pragma" content="no-cache">
        <meta name="keywords" content="IT Solution, SEO, Software, Performance, Startup, Mobile, Android, iPhone">
        <meta name="description" content="One stop IT solution for your business">
        <meta name="author" content="Sevenz Consultant">
        <meta name="copyright" content="&copy; 2014 - <?PHP echo date('Y'); ?>">

<!--        <link href="css/animate.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/imagelightbox.css" type="text/css" media="screen">
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <link href="css/mainmenu.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="js/woothemes-FlexSlider-06b12f8/flexslider.css" type="text/css" media="screen">
        <link rel="stylesheet" href="js/isotope/css/style.css" type="text/css" media="screen">
        <link href="css/simpletextrotator.css" rel="stylesheet" type="text/css">-->
        <link href="/css/minified.css" rel="stylesheet" type="text/css">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        
        <!--Modernizer Custom -->
        <script type="text/javascript" src="js/modernizr.custom.48287.js"></script>
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="apple-touch-fa-57x57-precomposed.png">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
    </head>
    <body id="onepage" class="sticky_header onepage" data-spy="scroll" data-target=".menu_container">
        <div class="overflow_wrapper">
            <header>
                <div class="container">
                    <div class="logo">
                        <a class="brand" href="#"> 
                            <img src="img/logo-small.jpg" alt="optional logo"> 
                            <span class="logo_title">Sevenz Consultant</span>
                        </a>
                    </div>
                    <div id="mainmenu" class="menu_container affix" role="complementary" data-spy="affix">
                        <label class="mobile_collapser">MENU</label>
                        <!-- Mobile menu title -->
                        <ul class="nav">
                            <li><a href="#onepage">Home</a></li>
                            <li><a href="#services">Services</a></li>
                            <li><a href="#portfolio">Portfolio</a></li>
                            <!--<li><a href="#about_us">About Us</a></li>-->
                            <li><a href="#contact">Contact</a></li>
                        </ul>
                    </div>
                    <div class="triangle-up-left"></div>
                    <div class="triangle-up-right"></div>
                </div>
            </header>
            <section id="slider_wrapper" class="slider_wrapper full_page_photo">
                <div id="main_flexslider" class="flexslider">
                    <ul class="slides">
                        <li class="item" style="background-image: url(img/slider3.jpg)">
                            <div class="container">
                                <div class="carousel-caption animated bounceInUp">
                                    <h1><strong>Solution</strong> driven</h1>
                                    <p class="lead skincolored">
                                        We bring solutions, not problem to your business.
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="item" style="background-image: url(img/slider2.jpg)">
                            <div class="container">
                                <div class="carousel-caption animated bounceInUp">
                                    <h1>Higher <strong>performance</strong></h1>
                                    <p class="lead skincolored">
                                        We handle your problem, you focus on performance and selling.
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="item" style="background-image: url(img/slider4.jpg)">
                            <div class="container">
                                <div class="carousel-caption animated bounceInUp">
                                    <h1><strong>Idea</strong> </h1>
                                    <p class="lead skincolored">
                                        Have an idea and want to run fast? We will empower your idea and make it happen.
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            <div class="main">
                <div class="container triangles-of-section">
                    <div class="triangle-up-left"></div>
                    <div class="square-left"></div>
                    <div class="triangle-up-right"></div>
                    <div class="square-right"></div>
                </div>
                <section class="call_to_action">
                    <div class="container">
                        <h3>Focus on what’s <strong><span class="rotate">Important, Urgent, Innovative</span></strong></h3>
                        <h4>and make your business run faster</h4>
                    </div>
                </section>
                <section id="services" class="features_teasers_wrapper">
                    <div class="container">
                        <h2 class="section_header fancy centered">
                            we design value into profit<small>our customers are always happy</small></h2>
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <div class="service_teaser vertical">
                                    <div class="service_photo">
                                        <figure style="background-image:url(img/serv_4.jpg)"></figure>
                                    </div>
                                    <div class="service_details">
                                        <h2>Design</h2>
                                        <p>
                                            We always focus on user experience for design and support all your devices, be it your tablet, phone, and PC.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="service_teaser vertical">
                                    <div class="service_photo">
                                        <figure style="background-image:url(img/serv_5.jpg)"></figure>
                                    </div>
                                    <div class="service_details">
                                        <h2>Development</h2>
                                        <p>
                                            We apply kanbanscrum methodology for our development process which is successfully meet our client's expectation.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="service_teaser vertical">
                                    <div class="service_photo">
                                        <figure style="background-image:url(img/serv_1.jpg)"></figure>
                                    </div>
                                    <div class="service_details">
                                        <h2>Support</h2>
                                        <p>
                                            We always give support to all our products, as we know your business is always improving. 
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="portfolio_teasers_wrapper" id="portfolio">
                    <div class="container">
                        <h2 class="section_header fancy centered">
                            Recent Work or Projects from Portfolio<small>we take pride in our work</small>
                        </h2>
                        <div class="portfolio_strict row">
                            <div class="col-sm-4 col-md-4">
                                <div class="portfolio_item wow flipInX"> 
                                    <a target="_blank" href="http://www.gala-wisata.co.id">
                                        <figure style="background-image:url(img/portfolio_gala.png)">
                                            <figcaption>
                                                <div class="portfolio_description">
                                                    <h3>PT. Duta GalaWisata</h3>
                                                    <span class="cross"></span>
                                                    <p>Travel Agent</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="portfolio_item wow flipInX"> 
                                    <a href="javascript:void(0)">
                                        <figure style="background-image:url(img/portfolio_whatiwear.png)">
                                            <figcaption>
                                                <div class="portfolio_description">
                                                    <h3>WhatIWear</h3>
                                                    <span class="cross"></span>
                                                    <p>Fashion Community Website (Retired)</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a> 
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="portfolio_item wow flipInX"> 
                                    <a target="_blank" href="http://legianbatujimbar.primagens.com">
                                        <figure style="background-image:url(img/portfolio_primagens.png)">
                                            <figcaption>
                                                <div class="portfolio_description">
                                                    <h3>CV Powerline</h3>
                                                    <span class="cross"></span>
                                                    <p>Genset Provider</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a> 
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="portfolio_item wow flipInX"> 
                                    <a target="_blank" href="http://trialgame.id">
                                        <figure style="background-image:url(img/portfolio_trialgame.png)">
                                            <figcaption>
                                                <div class="portfolio_description">
                                                    <h3>TrialGame</h3>
                                                    <span class="cross"></span>
                                                    <p>Motocross Competition</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a> 
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="portfolio_item wow flipInX"> 
                                    <a target="_blank" href="http://galawisata.id">
                                        <figure style="background-image:url(img/portfolio_galawisata2.png)">
                                            <figcaption>
                                                <div class="portfolio_description">
                                                    <h3>PT. DUTA GALAWISATA</h3>
                                                    <span class="cross"></span>
                                                    <p>Version 2.0</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <div class="portfolio_item wow flipInX"> 
                                    <a target="_blank" href="http://baliholidays.co.id">
                                        <figure style="background-image:url(img/portfolio_baliholidays.png)">
                                            <figcaption>
                                                <div class="portfolio_description">
                                                    <h3>PT. DUTA GALAWISATA</h3>
                                                    <span class="cross"></span>
                                                    <p>Version 2.0</p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
<!--                <section class="team_members" id="about_us">
                    <div class="container">
                        <h2 class="section_header fancy centered">
                            we really love what we do<small>while always trying to get the job done!</small>
                        </h2>
                        <div class="row team_members_wrapper">
                            <div class="col-sm-6 col-md-3">
                                <div class="team_member">
                                    <figure style="background-image: url(images/1b.jpg)"><img src="images/1a.jpg" alt="1a"></figure>
                                    <h5>Ihsan Kurniawan</h5>
                                    <small>CEO / Founder</small>
                                    <hr>
                                    <div class="team_social"> 
                                        <a href="https://www.facebook.com/ihsan.kurniawan.10">
                                            <i class="fa fa-facebook"></i></a> 
                                        <a href="https://twitter.com/1x4n">
                                            <i class="fa fa-twitter"></i></a> 
                                        <a href="https://www.linkedin.com/pub/ihsan-kurniawan/48/b0a/534">
                                            <i class="fa fa-linkedin"></i></a> 
                                        <a href="https://github.com/ihsankurniawan">
                                            <i class="fa fa-github-alt"></i></a> </div>
                                    <p class="short_bio">
                                        An IT enthusiast who always hunger for knowledge.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="clients_section wow fadeInUp">
                            <h2 class="section_header elegant centered">
                                Our clients <small>all of them are satisfied</small></h2>
                            <div class="clients_list"> 
                                <a href="#"><img src="images/clients/wordpress.png" alt="client"></a> 
                                <a href="#"><img src="images/clients/jquery.png" alt="client"></a> 
                                <a href="#"><img src="images/clients/microlancer.png" alt="client"></a> 
                                <a href="#"><img src="images/clients/bbpress.png" alt="client"></a> 
                                <a href="#"><img src="images/clients/wpml.png" alt="client"></a> 
                            </div>
                        </div>
                    </div>
                </section>-->

                <!-- TWITTER FEED SECTION -->

<!--                <section class="twitter_feed_wrapper skincolored_section">
                    <div class="container">
                        <div class="row">
                            <div class="twitter_feed_icon wow fadeInDown">
                                <a href="https://twitter.com/PlethoraThemes"><i class="fa fa-twitter"></i></a></div>
                            <div id="twitter_flexslider" class="flexslider"></div>
                        </div>
                    </div>
                </section>-->

                <!-- END OF TWITTER FEED SECTION -->

                <section id="contact">
                    <footer>
                        <section class="contact">
                            <div id="map"></div>
                            <div class="btn btn-primary-inv open_map"><i class="fa fa-flag"></i> show map</div>
                            <div class="btn btn-danger close_map"><i class="fa fa-flag"></i> Show Contact Form</div>
                            <div class="contact_form_overlay">
                                <div class="container">
                                    <div class="row">
                                        <div class="office_address col-sm-4 col-md-4">
                                            <div class="team_member"> 
                                                <img src="/img/logo.png"  alt="logo" style="width:50px;">
                                                <h5>Sevenz Consultant</h5>
                                                <small>Web-Development Company</small><br>
                                                <br>
                                                <address>
                                                    <strong></strong><br>
                                                    Golden Park<br>
                                                    Serpong Utara, Tangerang Selatan 15320<br>
                                                    <abbr title="Phone">P:</abbr> 0856 9120 8580
                                                </address>
                                                <address>
                                                    <abbr title="Email">E:</abbr> 
                                                    <a href="mailto:ihsan@sevenz-consultant.com">
                                                        ihsan@sevenz-consultant.com</a>
                                                </address>
                                            </div>
                                        </div>
                                        <div class="contact_form col-sm-8 col-md-8">
                                            <form name="contact_form" id="contact_form" method="post">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-6">
                                                        <label>Name</label>
                                                        <input name="name" id="name" class="form-control" type="text" value="">
                                                    </div>
                                                    <div class="col-sm-6 col-md-6">
                                                        <label>E-mail</label>
                                                        <input name="email" id="email" class="form-control" type="text" value="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-12">
                                                        <label>Subject</label>
                                                        <input name="subject" id="subject" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-12">
                                                        <label>Message</label>
                                                        <textarea name="message" id="message" rows="8" class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12"><br/>
                                                        <a id="submit_btn" class="btn btn-primary-inv" name="submit">Submit Message</a> <span id="notice" class="alert alert-warning alert-dismissable hidden" style="margin-left:20px;"></span> </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="copyright">
                            <div class="container triangles-of-section">
                                <div class="triangle-up-left"></div>
                                <div class="square-left"></div>
                                <div class="triangle-up-right"></div>
                                <div class="square-right"></div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6"> Copyright ©2014 - <?PHP echo date('Y'); ?> all rights reserved </div>
                                    <div class="text-right col-sm-6 col-md-6"></div>
                                </div>
                            </div>
                        </section>
                    </footer>
                </section>
            </div>
            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <!--<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>-->
            <script src="//cdnjs.cloudflare.com/ajax/libs/flexslider/2.1/jquery.flexslider-min.js"></script>
<!--            <script src="js/imagelightbox.min.js"></script>
            <script src="js/isotope/jquery.isotope.min.js" type="text/javascript"></script>
            <script src="js/jquery.ui.totop.js"></script>
            <script src="js/easing.js"></script>
            <script src="js/wow.min.js"></script>
            <script src="js/jquery.simple-text-rotator.js"></script>
            <script src="js/jquery-ui-1.8.22.custom.min.js"></script>
            <script src="js/jquery.imagesloaded.min.js"></script>-->
            
            <script src="js/minify-1.js"></script>
            
<!--            <script src="http://vjs.zencdn.net/4.3/video.js"></script>
            <script type="text/javascript" src="js/bigvideo.js"></script>
            <script type="text/javascript" src="js/bigvideo_init.js"></script>-->
            <!-- 
            You need to include the following 2 scripts on any page that has a Google Map BEFORE the Cleanstart_theme.js.
            -->
            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjHUBxfoPFRznRT6JvdBuekB9Q_GdgCaY&sensor=false"></script>
<!--            <script type="text/javascript" src="js/google_map.js"></script>
            <script type="text/javascript" src="js/main.js"></script>
            <script type="text/javascript" src="js/contact_form.js"></script>
            <script type="text/javascript" src="js/collapser.js"></script>-->
            <!--Twitter Feed API -->
            <!--<script type="text/javascript" src="js/tweetie/tweetie.min.js"></script>-->
            
            <script src="js/minify-2.js"></script>
        </div>
    </body>
</html>